

function hasLowerCase(str) {
    return (/[a-z]/.test(str));
}
function hasCapCase(str) {
    return (/[A-Z]/.test(str));
}
function numcheck(str) {
    return (/[1-9]/.test(str));
}
function namecheck(str){
 return /[\d~`!#$%\^&*+=\_\-\[\]\\';,/{}|\\":<>\?\s]/g.test(str);
}
function usernamecheck(str){
 return /[~`!#$%\^&*+=\[\]\\';,/{}|\\":<>\? ]/g.test(str);
}
function validateEmail(str) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(str);
}
function validateForm() {
	
	var x = document.forms["myForm"]["myName"].value;
    if (x.length < 3) {
        alert("Your name must have at least 3 letters");
        return false;
    }else if(namecheck(x))
    {
    	alert("Your name must not have special letters or number");
        return false;
    }
    
    var x = document.forms["myForm"]["lastname"].value;
    if (x.length < 3) {
        alert("Your lastname must have at least 3 letters");
        return false;
    }else if(namecheck(x))
    {
    	alert("Your lastname must not have special letters or number");
        return false;
    }

    var x = document.forms["myForm"]["username"].value;
    if (x.length < 3) {
        alert("Your Username must have at least 6 letters");
        return false;
    }else if(usernamecheck(x))
    {
    	alert("Your Username must not have special letters");
        return false;
    }

     var x = document.forms["myForm"]["pwd"].value;
    if (x.length <= 8) {
        alert("Your Password must have at least 8 letters");
        return false;
    }else if(!hasLowerCase(x)||!hasCapCase(x)||!namecheck(x)||!numcheck(x))
    {
    	alert("Your Password must contain Capital letters,Small letters,Special and Number");
        return false;
    }

     var y = document.forms["myForm"]["age"].value;
      if(y<18||y>110)
     {
     	alert("Do not allow people younger than 18 on this website!!");
        return false;
     }
    	var x = document.forms["myForm"]["emil"].value;
    var x = document.forms["myForm"]["email"].value;
	if (!validateEmail(x)) 
    {
        alert("Email – must be of the form abc@def.ghi");
        return false;
    }
	
    }
